locals {
  common_vars = yamldecode(file(("common_vars.yaml")))
}

generate "provider_version" {
  path      = "versions.tf"
  if_exists = "overwrite"
  contents = <<EOF
terraform {
  required_providers {
    aws = {
      source                = "hashicorp/aws"
      version               = "~> 4.65"
    }
}
}
EOF
}

generate "backend" {
  path      = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
terraform {
  backend "s3" {
    bucket         = "${local.common_vars.company_prefix}-${local.common_vars.env}-${local.common_vars.state_bucket}"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "${local.common_vars.region}"
  }
}
EOF
}