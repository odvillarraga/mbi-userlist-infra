locals {
  common_vars = yamldecode(file(find_in_parent_folders("common_vars.yaml")))
}

terraform {
  source = "../../../modules/website"
}

inputs = {
  bucket_name = "${local.common_vars.env}.${local.common_vars.company_prefix}.userlist.oscarvillarraga.com"
}
