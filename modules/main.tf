
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.65"
    }
  }
}


module "website" {
  source = "./website"
  bucket_name = "test.oscarvillarraga.com"
}